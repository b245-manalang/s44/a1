const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const orderController = require('../Controllers/orderController.js');

// Route for adding an order
router.post('/', auth.verify, orderController.createOrder);




module.exports = router;