const Order = require('../Models/orderSchema.js');
const auth = require('../auth.js');

// Non-admin User checkout (Create Order)
module.exports.createOrder = (req, res) =>{
	let input = req.body;
	let userData = auth.decode(req.headers.authorization);

	let newOrder = new Order({
		userId: input.userId,
		productId: input.productId,
		quantity: input.quantity		
	});

	if(!userData.isAdmin){
		
		return newOrder.save()
		
		.then(order =>{
			console.log(order);
			res.send('Your order has been created!');
		})
		
		.catch(error =>{
			console.log(error);
			res.send(error);
		})
	}else{
		return res.send('You are an admin!')
	}
	
}

		