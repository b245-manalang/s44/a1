const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');

const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Controllers

//User Registration
module.exports.userRegistration = (req, res) => {

		const input = req.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return res.send('The email is already taken!')
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})

				newUser.save()
				.then(save => {
					return res.send('You are now registered to our website!')
							})
				.catch(err => {
					return res.send(err)
				})
			}
		})

		.catch(err => {
			return res.send(err)
		})
}

//User Authentication 	
module.exports.userAuthentication = (req, res) => {
		let input = req.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return res.send('Email is not yet registered. Register first before logging in!')
			}else{
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return res.send({auth: auth.createAccessToken(result)});
				}else{
					return res.send('Password is incorrect!')
				}
			}
		})

		.catch(err => {
			return res.send(err)
		})
}

