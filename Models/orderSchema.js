const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'UserId is required!']
	},
	productId: {
		type: String,
		required: [true, 'Product Id is required!']
	},
	quantity: {
		type: Number,
		default: 0
	},
	totalAmount: {
		type: Number,
		default: 0
	
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model('Order', orderSchema);